﻿using PKHeX.Core;

ushort[] TMHM_SM =
[
    526, 337, 473, 347, 046, 092, 258, 339, 474, 237,
    241, 269, 058, 059, 063, 113, 182, 240, 355, 219,
    218, 076, 479, 085, 087, 089, 216, 141, 094, 247,
    280, 104, 115, 482, 053, 188, 201, 126, 317, 332,
    259, 263, 488, 156, 213, 168, 490, 496, 497, 315,
    211, 411, 412, 206, 503, 374, 451, 507, 693, 511,
    261, 512, 373, 153, 421, 371, 684, 416, 397, 694,
    444, 521, 086, 360, 014, 019, 244, 523, 524, 157,
    404, 525, 611, 398, 138, 447, 207, 214, 369, 164,
    430, 433, 528, 057, 555, 267, 399, 127, 605, 590,

    // No HMs
];

var pt = PersonalTable.USUM;
var learnsets = LearnSource7USUM.Instance;
var evos = EvolutionTree.Evolves7.Forward;
var tms = TMHM_SM;

var s = GameInfo.Strings.Species.ToArray();

var altForms = GetFormList(pt, s);

var dumper = new PersonalDumper.PersonalDumper
{
    Abilities = GameInfo.Strings.abilitylist,
    Types = GameInfo.Strings.types,
    Colors = Enum.GetNames(typeof(PokeColor)),
    EggGroups = Enum.GetNames(typeof(EggGroup)),
    ExpGroups = Enum.GetNames(typeof(EXPGroup)),
    Moves = GameInfo.Strings.Move,
    Species = s,

    EntryLearnsets = learnsets,
    Evos = evos,
    TMIndexes = tms,
};

var result = dumper.Dump(pt);

File.WriteAllLines("usum.txt", result);

return;

static string[][] GetFormList(IPersonalTable pt, string[] species)
{
    string[][] FormList = new string[pt.MaxSpeciesID + 1][];
    for (int i = 0; i < FormList.Length; i++)
    {
        int FormCount = pt[i].FormCount;
        FormList[i] = new string[FormCount];
        if (FormCount <= 0) continue;
        FormList[i][0] = species[i];
        for (int j = 1; j < FormCount; j++)
            FormList[i][j] = $"{species[i]} {j}";
    }

    return FormList;
}

public enum PokeColor
{
    Red,
    Blue,
    Yellow,
    Green,
    Black,
    Brown,
    Purple,
    Gray,
    White,
    Pink,
}

public enum EggGroup
{
    None,
    Monster,
    Water1,
    Bug,
    Flying,
    Field,
    Fairy,
    Grass,
    HumanLike,
    Water3,
    Mineral,
    Amorphous,
    Water2,
    Ditto,
    Dragon,
    Undiscovered,
}

public enum EXPGroup
{
    MediumFast,
    Erratic,
    Fluctuating,
    MediumSlow,
    Fast,
    Slow,
}
