using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PKHeX.Core;

namespace PersonalDumper;

public class PersonalDumperSettings
{
    public bool Stats { get; set; } = true;
    public bool Learn { get; set; } = true;
    public bool Egg { get; set; } = true;
    public bool TMHM { get; set; } = true;
    public bool Tutor { get; set; } = true;
    public bool Evo { get; set; } = true;
    public bool Dex { get; set; } // Skipped to reduce dumped file size
}

public class PersonalDumper
{
    public bool HasAbilities { get; set; } = true;
    public bool HasItems { get; set; } = true;

    public IReadOnlyList<string> Abilities { private get; set; } = [];
    public IReadOnlyList<string> Types { private get; set; } = [];
    public IReadOnlyList<string> Items { private get; set; } = [];
    public IReadOnlyList<string> Colors { private get; set; } = [];
    public IReadOnlyList<string> EggGroups { private get; set; } = [];
    public IReadOnlyList<string> ExpGroups { private get; set; } = [];
    public IReadOnlyList<string> Moves { protected get; set; } = [];
    public IReadOnlyList<string> Species { private get; set; } = [];
    public IReadOnlyList<string> ZukanA { private get; set; } = [];
    public IReadOnlyList<string> ZukanB { private get; set; } = [];

    public ILearnSource EntryLearnsets { private get; set; } = null;
    public IEvolutionForward Evos { private get; set; } = null;
    public IReadOnlyList<ushort> TMIndexes { protected get; set; } = [];
    public IReadOnlyList<ushort> TRIndexes { protected get; set; } = [];

    private static readonly string[] AbilitySuffix = [" (1)", " (2)", " (H)"];
    private static readonly string[] ItemPrefix = ["Item 1 (50%)", "Item 2 (5%)", "Item 3 (1%)"];

    public IReadOnlyList<List<string>> MoveSpeciesLearn { get; private set; } = [];

    public PersonalDumperSettings Settings = new();

    public List<string> Dump(IPersonalTable table)
    {
        var lines = new List<string>();
        var ml = new List<string>[Moves.Count];
        for (int i = 0; i < ml.Length; i++)
            ml[i] = [];
        MoveSpeciesLearn = ml;

        for (ushort species = 0; species <= table.MaxSpeciesID; species++)
        {
            var spec = table[species];
            for (byte form = 0; form < spec.FormCount; form++)
                if (table.IsPresentInGame(species, form))
                    AddDump(lines, table, species, form);
        }
        return lines;
    }

    public void AddDump(List<string> lines, IPersonalTable table, ushort species, byte form)
    {
        var index = table.GetFormIndex(species, form);
        var entry = table[index];
        string name = Species[species];
        if (form > 0) name += $" {form}";
        AddDump(lines, entry, index, name, species, form);
        lines.Add("");
    }

    private void AddDump(List<string> lines, IPersonalInfo pi, int entry, string name, int species, int form)
    {
        var specCode = pi.FormCount > 1 ? $"{Species[species]}-{form}" : $"{Species[species]}";

        if (Settings.Stats)
            AddPersonalLines(lines, pi, entry, name, specCode);
        if (Settings.Learn)
            AddLearnsets(lines, (ushort) species, (byte) form, specCode);
        if (Settings.Egg)
            AddEggMoves(lines, (ushort) species, (byte) form, specCode);
        if (Settings.TMHM && pi is IPersonalInfoTM mi)
            AddTMs(lines, mi, specCode);
        if (Settings.TMHM && pi is IPersonalInfoTR mitr)
            AddTRs(lines, mitr, specCode);
        if (Settings.Tutor && pi is IPersonalInfoTutorType mi2)
            AddArmorTutors(lines, mi2, specCode);
        if (Settings.Evo)
            AddEvolutions(lines, (ushort) species, (byte) form);
        if (Settings.Dex)
            AddZukan(lines, entry);
    }

    private void AddZukan(List<string> lines, int entry)
    {
        if (entry >= Species.Count)
            return;
        //lines.Add(ZukanA[entry].Replace("\\n", " "));
        //lines.Add(ZukanB[entry].Replace("\\n", " "));
    }

    protected virtual void AddTMs(List<string> lines, IPersonalInfoTM tmhm, string SpecCode)
    {
        int count = 0;
        lines.Add("TMs:");
        for (int i = 0; i < 100; i++)
        {
            if (!tmhm.GetIsLearnTM(i))
                continue;
            var move = TMIndexes[i];
            lines.Add($"- [TM{i + 1:00}] {Moves[move]}");
            count++;

            MoveSpeciesLearn[move].Add(SpecCode);
        }
        if (count == 0)
            lines.Add("None!");
    }

    protected virtual void AddTRs(List<string> lines, IPersonalInfoTR tr, string SpecCode)
    {
        int count = 0;
        lines.Add("TRs:");
        for (int i = 0; i < 100; i++)
        {
            if (!tr.GetIsLearnTR(i))
                continue;
            var move = TRIndexes[i];
            lines.Add($"- [TR{i + 1:00}] {Moves[move]}");
            count++;

            MoveSpeciesLearn[move].Add(SpecCode);
        }
        if (count == 0)
            lines.Add("None!");
    }

    protected virtual void AddArmorTutors(List<string> lines, IPersonalInfoTutorType armor, string SpecCode)
    {
        int count = 0;
        lines.Add("Armor Tutors:");
        for (int i = 0; i < 100; i++)
        {
            if (!armor.GetIsLearnTutorType(i))
                continue;
            //var move = Legal.Tutors_SWSH_1[i];
            //lines.Add($"- {Moves[move]}");
            lines.Add($"- #{i}"); // TODO: not this
            count++;

            //MoveSpeciesLearn[move].Add(SpecCode);
        }
        if (count == 0)
            lines.Add("None!");
    }

    private void AddLearnsets(List<string> lines, ushort species, byte form, string specCode)
    {
        var learn = EntryLearnsets.GetLearnset(species, form);
        if (learn.GetAllMoves().Length == 0)
            return;

        lines.Add("Level Up Moves:");
        for (int i = 0; i < learn.GetAllMoves().Length; i++)
        {
            var move = learn.GetAllMoves()[i];
            var level = learn.GetLevelLearnMove(move);
            lines.Add($"- [{level:00}] {Moves[move]}");
            MoveSpeciesLearn[move].Add(specCode);
        }
    }

    private void AddEggMoves(List<string> lines, ushort species, byte form, string specCode)
    {
        var egg = EntryLearnsets.GetEggMoves(species, form);
        if (egg.Length == 0)
            return;

        lines.Add("Egg Moves:");
        foreach (var move in egg)
        {
            lines.Add($"- {Moves[move]}");
            MoveSpeciesLearn[move].Add(specCode);
        }
    }

    private void AddEvolutions(List<string> lines, ushort species, byte form)
    {
        var evo = Evos.GetForward(species, form).ToArray();
        var evo2 = evo.Where(z => z.Species != 0).ToArray();
        if (evo2.Length == 0)
            return;

        var msg = evo2.Select(z => $"Evolves into {Species[z.Species]}-{z.Form} @ {z.Level} ({z.Method}) [{z.Argument}]");
        lines.AddRange(msg);
    }

    private void AddPersonalLines(List<string> lines, IPersonalInfo pi, int entry, string name, string specCode)
    {
        Debug.WriteLine($"Dumping {specCode}");
        lines.Add("======");
        lines.Add($"{entry:000} - {name} (Stage: {pi.EvoStage})");
        lines.Add("======");
        if (pi is PersonalInfo8SWSH s)
        {
            if (s.PokeDexIndex != 0)
                lines.Add($"Galar Dex: #{s.PokeDexIndex:000}");
            if (s.ArmorDexIndex != 0)
                lines.Add($"Armor Dex: #{s.ArmorDexIndex:000}");
            if (s.CrownDexIndex != 0)
                lines.Add($"Crown Dex: #{s.CrownDexIndex:000}");
            if (s is { PokeDexIndex: 0, ArmorDexIndex: 0, CrownDexIndex: 0 })
                lines.Add("Galar Dex: Foreign");

            //if (s.CanNotDynamax)
            //    lines.Add("Can Not Dynamax!");
        }
        lines.Add($"Base Stats: {pi.HP}.{pi.ATK}.{pi.DEF}.{pi.SPA}.{pi.SPD}.{pi.SPE} (BST: {pi.GetBaseStatTotal()})");
        lines.Add($"EV Yield: {pi.EV_HP}.{pi.EV_ATK}.{pi.EV_DEF}.{pi.EV_SPA}.{pi.EV_SPD}.{pi.EV_SPE}");
        lines.Add($"Gender Ratio: {pi.Gender}");
        lines.Add($"Catch Rate: {pi.CatchRate}");

        if (HasAbilities)
        {
            string msg = string.Empty;
            for (int j = 0; j < pi.AbilityCount; ++j)
                msg += Abilities[pi.GetAbilityAtIndex(j)] + AbilitySuffix[j] + " | ";

            lines.Add($"Abilities: {msg}");
        }

        lines.Add(string.Format(pi.Type1 != pi.Type2
            ? "Type: {0} / {1}"
            : "Type: {0}", Types[(int)pi.Type1], Types[(int)pi.Type2]));

        //if (HasItems)
        //{
        //    int[] items = new int[pi.GetNumItems()];
        //    pi.GetItems(items);
        //    if (items.Distinct().Count() == 1)
        //        lines.Add($"Items: {Items[pi.Item1]}");
        //    else
        //        lines.AddRange(items.Select((z, j) => $"{ItemPrefix[j]}: {Items[z]}"));
        //}

        lines.Add($"EXP Group: {ExpGroups[pi.EXPGrowth]}");
        lines.Add(string.Format(pi.EggGroup1 != pi.EggGroup2
            ? "Egg Group: {0} / {1}"
            : "Egg Group: {0}", EggGroups[pi.EggGroup1], EggGroups[pi.EggGroup2]));

        lines.Add($"Hatch Cycles: {pi.HatchCycles}");

        lines.Add($"Height: {(decimal)pi.Height / 100:00.00}m, Weight: {(decimal)pi.Weight / 10:000.0}kg, Color: {Colors[pi.Color]}");
    }
}
